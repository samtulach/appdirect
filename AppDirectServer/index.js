const express = require('express')
const app = express()
const port = 3000

var api = require('gpapi').GooglePlayAPI({
    username: "login",
    password: "passwd",
    androidId: "id"
})

function getAppDetails (resp, pkg) {
    return api.details(pkg).then(function (res) {
      console.log(JSON.stringify(res, null, 4));
      resp.send(res);
    });
}

function getDownloadInfo (res, pkg, vc) {
    return api.login()
      .then(function () {
        api.details(pkg).then(function (res) {
          return res.details.appDetails.versionCode;
        })
          .then(function (versionCode) {
            if (vc) {
              return api.downloadInfo(pkg, vc);
            } else {
              return api.downloadInfo(pkg, versionCode);
            }
          })
          .then(function (info) {
            console.log('%j', info);
            res.send(info);
          });
      });
}

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.get('/download/:appID', function (req, res) {
    getDownloadInfo(res, req.params.appID);
})

app.get('/details/:appID', function (req, res) {
    getAppDetails(res, req.params.appID);
})

app.listen(port, () => console.log(`Server is running on port ${port}!`))